$(document).ready(function () {


  /* scroll to */

  $("a.scrollto").click(function () {
    $("html, body").animate({
      scrollTop: $($(this).attr("href")).offset().top + "px"
    }, {
      duration: 800,
      easing: "swing"
    });
    return false;
  });

  /* end scrollto */

  /* hamburger */

  $('.hamburger').click(function () {
    $(this).toggleClass('is-active');
    $('.xs-menu').toggleClass('xs-menu_active');
    $('body').toggleClass('body-ofh');
  });

  /* house slider */

  $('.house-slider').slick({
    prevArrow: '<svg class="slick-prev" role="img" width="42" height="25"><use xlink:href="assets/images/sprite.svg#arrow"></use></svg>',
    nextArrow: '<svg class="slick-next" role="img" width="42" height="25"><use xlink:href="assets/images/sprite.svg#arrow"></use></svg>',
    dots: true,
    appendArrows: '.house-slider__nav',
    appendDots: '.house-slider__nav'
  });

  /* house slider end */

 

  /* phone mask */

  $('.footer-form__input').inputmask("+7 (999) 999-99-99");

  /* end phone mask */


  /* progress-circle */

  


  /* end progress-circle */


  /* adaptive header */
  if($(window).width() < 1200){
    var $header = $('header'),
        $headerPhone = $('.header-menu__phone');

    $('body').prepend($header);
    $('.header-menu .logo').after($headerPhone);
  }
  


  /* end adaptive header */
  



});