
// Initialize and add the map
function initMap() {
  // The location of Uluru
  var uluru = {
    lat: 59.2290970,
    lng: 39.9081890
  };

  /*styles */

  var styles = [{
    "stylers": [{
      "saturation": -100
    }]
  }, {
    "featureType": "administrative",
    "stylers": [{
      "visibility": "off"
    }]
  }, {
    "featureType": "poi",
    "stylers": [{
      "visibility": "off"
    }]
  }];

  /* end styles */
  // The map, centered at Uluru
  var map = new google.maps.Map(
    document.getElementById('map'), {
      zoom: 16,
      center: uluru,
      styles: styles
    });
  // The marker, positioned at Uluru
  image = 'assets/images/ymap-icon.png';
  var marker = new google.maps.Marker({
    position: uluru,
    map: map,
    icon: image
  });


  var Circle5min = new google.maps.Circle({
    strokeColor: '#F1A23A',
    strokeOpacity: .7,
    strokeWeight: 1,
    fillColor: 'transparent',
    fillOpacity: 1,
    map: map,
    center: uluru,
    radius: 300
  });

  var Circle10min = new google.maps.Circle({
    strokeColor: '#F1A23A',
    strokeOpacity: .7,
    strokeWeight: 1,
    fillColor: 'transparent',
    fillOpacity: 1,
    map: map,
    center: uluru,
    radius: 600
  });

  var Circle15min = new google.maps.Circle({
    strokeColor: '#F1A23A',
    strokeOpacity: .7,
    strokeWeight: 1,
    fillColor: 'transparent',
    fillOpacity: 1,
    map: map,
    center: uluru,
    radius: 900
  });


  /*places*/
  
  
  var json = [{
      "title": "Киношки",
      "description": "",
      "type": "culture",
      "icon": "assets/images/culture-icon.png",
      "lat": 59.2301529,
      "lng": 39.9070598
    }, {
      "title": "Кафе \"Подлива\"",
      "description": "",
      "type": "cafe",
      "icon": "assets/images/cafe-icon.png",
      "lat": 59.2295166,
      "lng": 39.9058037
    }, {
      "title": "С.Кубрик - Сияние",
      "description": "",
      "type": "medicine",
      "icon": "assets/images/medicine-icon.png",
      "lat": 59.2288111,
      "lng": 39.9069986
    }, {
      "title": "Покрышки",
      "description": "",
      "type": "shop",
      "icon": "assets/images/shop-icon.png",
      "lat": 59.2296511,
      "lng": 39.9090964
    }, {
      "title": "Шишки",
      "description": "",
      "type": "school",
      "icon": "assets/images/education-icon.png",
      "lat": 59.2304079,
      "lng": 39.9094156
    },
    {
      "title": "ЖЪЖЖЖЪЖ",
      "description": "",
      "type": "sport",
      "icon": "assets/images/sport-icon.png",
      "lat": 59.2298217,
      "lng": 39.9063046
    }
  ];

  function attachMessage(marker, Message) {
    var infowindow = new google.maps.InfoWindow({
      content: Message
    });

    marker.addListener('click', function () {
      infowindow.open(marker.get('map'), marker);
    });
  }

  var placeMarkers = [];

  for (var i = 0, length = json.length; i < length; i++) {
    var data = json[i],
      latLng = new google.maps.LatLng(data.lat, data.lng);

    // Creating a marker and putting it on the map
    
    var placeMarker = new google.maps.Marker({
      position: latLng,
      map: map,
      type: data.type,
      title: data.title,
      icon: data.icon
    });

    attachMessage(placeMarker, data.title);
    
    placeMarkers.push(placeMarker);
  }

  

  /* filter for places */

  $('.maps__filter input[type="checkbox"]').change(function(){
    
    $('.maps__filter input[type="checkbox"]').each(function(i,elem){
      var type = $(elem).val();

      if( $(this).prop('checked') ){
        for (i = 0; i < placeMarkers.length; i++) {
          marker = placeMarkers[i];
          if (marker.type == type || type.length == 0) {
            marker.setVisible(true);
          } 
        }
      } else {
        for (i = 0; i < placeMarkers.length; i++) {
          marker = placeMarkers[i];
          if (marker.type == type || type.length == 0) {
            marker.setVisible(false);
          }
        }
      }
    });
   
  });

  $('.maps__filter input[type="checkbox"]').each(function (i, elem) {
    var type = $(elem).val();

    if ($(this).prop('checked')) {
      for (i = 0; i < placeMarkers.length; i++) {
        marker = placeMarkers[i];
        if (marker.type == type || type.length == 0) {
          marker.setVisible(true);
        }
      }
    } else {
      for (i = 0; i < placeMarkers.length; i++) {
        marker = placeMarkers[i];
        if (marker.type == type || type.length == 0) {
          marker.setVisible(false);
        }
      }
    }
  });

  /*end places*/

  

  /* info-window */

  
}